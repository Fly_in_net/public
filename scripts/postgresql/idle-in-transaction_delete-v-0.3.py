#!/usr/bin/env python3
'''Подключаем необходимые библиотеки, модуль для работы с postgreSQL, определяем курсор, модуль для регулярных выражений.'''
import psycopg2             #Postgres api
from psycopg2 import sql    #Postgres api
import re                   #Регулярные выражения
import os                   #Работа с вызовами ОС, я использовал для чтения файлов.
import sys                  #Системные вызовы, я использовал для вызова экстренного выходв.
import subprocess           #Передача вызова через оболочку (Своего рода)
'''В файле для подключения к системам находим необходимые данные: Сервер, логин, пароль'''
f=open('properties')

for line in f:
        if "db-myhome.jdbc" in line:
                hostname=line.split('/')[2]
                hostname=re.sub("^\s+|:5432|\n|\r|\s+$", '',hostname)
                db_name=line.split('/')[3]
                db_name=re.sub("^\s+|\n|\r|\s+$", '',db_name)
        elif "db-myhome-rep.jdbc" in line:
                hostname_rep=line.split('/')[2]
                hostname_rep=re.sub("^\s+|:5432|\n|\r|\s+$", '',hostname_rep)
        elif "myhome.db.user" in line:
                username=line.split("=")[1]
                username=re.sub("^\s+|\n|\r|\s+$", '',username)
        elif "myhome.db.password" in line:
                passwd=line.split("=")[1]
                passwd=re.sub("^\s+|\n|\r|\s+$", '',passwd)

#Создаем переменные и записываем в них запросы параметрами:
sql_0='''select pid from pg_stat_activity  where (state = 'idle in transaction') and xact_start is not null and (current_timestamp-xact_start) > interval '30 minutes';'''
sql_1='''select pg_terminate_backend('%s');'''
count_r_t=0 #Успешное завершение операции, Реплика
count_p_t=0 #Успешное завершение операции, Бой
count_r_f=0 #Неудачное завершение операции, Реплика
count_p_f=0 #Неудачное завершение операции, Бой
'''Подключаемся к БД rep'''
try:
    conn=psycopg2.connect(dbname=db_name, user=username, host=hostname_rep, port=5432, password=passwd)
#'''Находим зависшие транзакции:'''
    cursor=conn.cursor()
#Исполняем sql_0 и сохраняем параметры в переменную:
    cursor.execute(sql_0)
    data_0=cursor.fetchall()
    if len(data_0)>0:
        for data_1 in data_0:
            cursor.execute(sql_1, data_1)
            if cursor.fetchone()[0] == False:
                count_r_f=count_r_f+1
            else:
                count_r_t=count_r_t+1
#Информирование в чат
    if (count_r_f>0 and count_r_t>0):
        subprocess.call("../telegram_bot.sh '%s' '%s'" % ("Rep: Error: ", str(count_r_f)), shell=True)
        subprocess.call("../telegram_bot.sh '%s' '%s'" % ("Rep: Удалено зависших транзакций: ", str(count_r_t)), shell=True)
    elif count_r_f > 0: 
        subprocess.call("./telegram_bot.sh '%s' '%s'" % ("Rep: Error: ", str(count_r_f)), shell=True)
    elif count_r_t > 0:
        subprocess.call("../telegram_bot.sh '%s' '%s'" % ("Rep: Удалено зависших транзакций: ", str(count_r_t)), shell=True)
    else:
         subprocess.call("../telegram_bot.sh '%s' '%s'" % ("Rep: Удалено зависших транзакций: ", str(count_r_t)), shell=True)
except psycopg2.OperationalError as e:
    tmp=('Unable to connect!\n{0}').format(e)
    subprocess.call("../telegram_bot.sh '%s' '%s'" % ("Проверка зависших транзакций не удалась. Ошибка: ", tmp), shell=True)
    sys.exit(1)
else:
    cursor.close()
    conn.close()

'''Подключаемся к БД prod'''
try:
    conn=psycopg2.connect(dbname=db_name, user=username, host=hostname, port=5432, password=passwd)
#'''Находим зависшие транзакции:'''
    cursor=conn.cursor()
#Исполняем sql_0 и сохраняем параметры в переменную:
    cursor.execute(sql_0)
    data_0=cursor.fetchall()
    if len(data_0)>0:
        for data_1 in data_0:
            cursor.execute(sql_1, data_1)
            if cursor.fetchone()[0] == False:
                count_p_f=count_r_f+1
            else:
                count_p_t=count_r_t+1
#Информирование в чат
    if count_r_f > 0:
        subprocess.call("./telegram_bot.sh '%s' '%s'" % ("Prod: Error: ", str(count_p_f)), shell=True)
    elif (count_r_f>0 and count_r_t>0):
         subprocess.call("./telegram_bot.sh '%s' '%s'" % ("Prod: Error: ", str(count_p_f)), shell=True)
         subprocess.call("./telegram_bot.sh '%s' '%s'" % ("Prod: Удалено зависших транзакций: ", str(count_p_t)), shell=True)
    else:
         subprocess.call("./telegram_bot.sh '%s' '%s'" % ("Prod: Удалено зависших транзакций: ", str(count_p_t)), shell=True)
except psycopg2.OperationalError as e:
    print('Unable to connect!\n{0}').format(e)
    sys.exit(1)
else:
    cursor.close()
    conn.close()
#Для того чтобы работали оповещения в чат телеграм вам нужен телеграм бот. Я писал bash.
#Для того чтобы работал блок с парсом данных Необходимсо файл properties привести к следующему виду:
'''
db-myhome.jdbcUrl=jdbc:postgresql://<host>:5432/myhomedb
db-myhome-rep.jdbcUrl=jdbc:postgresql://<host>:5432/myhomedb
myhome.db.user=user
myhome.db.password=password
'''
''' P.S. : Да,я понимаю, что можно было бы написать функцию, классы... Но сейчас я уже знаю как писать функции, однако еще не одной не написал.
Я только учусь. Я решил действовать по принципу: напиши рабочий код внедри его. А рефакторингом будешь заниматься тогда, код перестанет работать.
Это нужно для того, чтобы развиваться не только в языке, но и в логике, алгоритмике и прочих направлениях. Так что какие то новые плюшки по языку,
в моем исполнении можно будет увидеть только в следующих моих работах.'''