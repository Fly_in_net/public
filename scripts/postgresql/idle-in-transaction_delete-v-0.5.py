#!/usr/bin/env python3
import psycopg2
from psycopg2 import sql
import re
import os
import sys
import subprocess
#В файле для подключения к системам находим необходимые данные: Сервер, логин, пароль
f=open('../auditor.properties')

for line in f:
        if "db-myhome.jdbc" in line:
                hostname=line.split('/')[2]
                hostname=re.sub("^\s+|:5432|\n|\r|\s+$", '',hostname)
                db_name=line.split('/')[3]
                db_name=re.sub("^\s+|\n|\r|\s+$", '',db_name)
        elif "db-myhome-rep.jdbc" in line:
                hostname_rep=line.split('/')[2]
                hostname_rep=re.sub("^\s+|:5432|\n|\r|\s+$", '',hostname_rep)
        elif "myhome.db.user" in line:
                username=line.split("=")[1]
                username=re.sub("^\s+|\n|\r|\s+$", '',username)
        elif "myhome.db.password" in line:
                passwd=line.split("=")[1]
                passwd=re.sub("^\s+|\n|\r|\s+$", '',passwd)

#Создаем переменные и записываем в них запросы параметрами:
sql_0='''select pid,usename from pg_stat_activity  where (state = 'idle in transaction') and xact_start is not null and (current_timestamp-xact_start) > interval '30 minutes';'''
sql_1='''select pg_terminate_backend('%s');'''
count_r_t=0 #Успешное завершение операции, Реплика
count_p_t=0 #Успешное завершение операции, Бой
count_r_f=0 #Неудачное завершение операции,  Реплика
count_p_f=0 #Неудачное завершение операции, Бой
result_p_fail=[] #Информирование прод провал
result_p_OK=[]   #Информирование прод успех
result_r_fail=[] #Информирование реплика провал
result_r_OK=[]   #Информирование реплика успех
'''Подключаемся к БД rep'''
try:
    conn=psycopg2.connect(dbname=db_name, user=username, host=hostname_rep, port=5432, password=passwd)
#Находим зависшие транзакции:
    cursor=conn.cursor()
    cursor.execute(sql_0)
    data_0=cursor.fetchall()
#Удаляем транзакции:
    if any(data_0):
        for data_1 in data_0:
            cursor.execute(sql_1, [data_1[0]])
            if cursor.fetchone()[0] == False:
                result_r_fail.append(data_1)
                count_r_f=count_r_f+1
            else:
                result_r_OK.append(data_1)
                count_r_t=count_r_t+1
#Информируем в телеграм:
    if count_r_f > 0:
        subprocess.call("/myhomedb/telegram_bot.sh '%s' '%s'" % ("Rep.idle-in-transaction_delete.Error: {}\nError when deleting transactions: ".format(str(count_r_f)),str(result_r_fail)), shell=True)
    elif count_r_t > 0:
        subprocess.call("/myhomedb/telegram_bot.sh '%s' '%s'" % ('Rep.idle-in-transaction_delete The following transactions have been deleted: {}\n Counts deleted transactions: '.format(str(result_r_OK)),str(count_r_t)), shell=True)
except psycopg2.OperationalError as e:
    tmp=('Unable to connect!\n{0}').format(e)
    subprocess.call("/myhomedb/telegram_bot.sh '%s' '%s'" % ("Ошибка при выполнении скрипта:",str(tmp)), shell=True)
    sys.exit(1)
else:
    cursor.close()
    conn.close()

'''Подключаемся к БД prod'''
try:
    conn=psycopg2.connect(dbname=db_name, user=username, host=hostname, port=5432, password=passwd)
#Находим зависшие транзакции:
    cursor=conn.cursor()
    cursor.execute(sql_0)
    data_0=cursor.fetchall()
#Удаляем транзакции:
    if any(data_0):
        for data_1 in data_0:
            cursor.execute(sql_1, [data_1[0]])
            if cursor.fetchone()[0] == False:
                result_p_fail.append(data_1)
                count_p_f=count_p_f+1
            else:
                result_p_OK.append(data_1)
                count_p_t=count_p_t+1
#Информируем в телеграм:
    if count_p_f > 0:
        subprocess.call("/myhomedb/telegram_bot.sh '%s' '%s'" % ("Rep.idle-in-transaction_delete.Error: {}\nError when deleting transactions: ".format(str(count_p_f)),str(result_p_fail)), shell=True)
    elif count_p_t > 0:
        subprocess.call("/myhomedb/telegram_bot.sh '%s' '%s'" % ('Prod.idle-in-transaction_delete The following transactions have been deleted: {}\n Counts deleted transactions: '.format(str(result_r_OK)),str(count_p_t)), shell=True)
except psycopg2.OperationalError as e:
    tmp=('Unable to connect!\n{0}').format(e)
    subprocess.call("/myhomedb/telegram_bot.sh '%s' '%s'" % ("Ошибка при выполнении скрипта:",str(tmp)), shell=True)
    sys.exit(1)
else:
    cursor.close()
    conn.close()
