#!/bin/bash

TOKEN=<telegram_token>
CHAT=<telegram_chat_id>
b_time=$(date +%d.%m)
find <Каталог, куда будут складываться данные>/zabbix-backup/ -type f -mtime 0 -delete
$(sudo -u postgres pg_dump -T 'event*' -T 'history*' -T 'trend*' -d zabbix > <Каталог для backup>/$b_time.zabbix.schema.sql && sudo -u postgres pg_dump -U postgres -d zabbix -s > <Каталог для backup>/$b_time.zabbix.shcema_backup.sql)
tmp=$(echo $?)
if [ $tmp != 0 ]; then
        curl -s https://api.telegram.org/bot${TOKEN}/sendMessage -d "chat_id=$CHAT" -d "text=ERROR ZabbixDB.backup завершился ошибкой!!! \nHost:<Имя хоста забикс>}"
        return 1
fi

$(cp -r /etc/zabbix/* <Каталог для backup>/backup/etc_zabbix/)
tmp=$(echo $?)
if [ $tmp != 0 ]; then
        curl -s https://api.telegram.org/bot${TOKEN}/sendMessage -d "chat_id=$CHAT" -d "text=ERROR ZabbixDB.backup завершился ошибкой корования директории '/etc/zabbix/'!!! \nHost:<Имя хоста забикс>}"
        return 1
fi

$(cp -r /usr/share/zabbix/* <Каталог для backup>/backup/usr_share_zabbix/)
tmp=$(echo $?)
if [ $tmp != 0 ]; then
        curl -s https://api.telegram.org/bot${TOKEN}/sendMessage -d "chat_id=$CHAT" -d "text=ERROR ZabbixDB.backup завершился ошибкой корования директории '/usr/share/zabbix/'!!! \nHost:<Имя хоста забикс>}"
        return 1
fi

$(cp -r /etc/apache2/* <Каталог для backup>/backup/etc_apache2/)
tmp=$(echo $?)
if [ $tmp != 0 ]; then
        curl -s https://api.telegram.org/bot${TOKEN}/sendMessage -d "chat_id=$CHAT" -d "text=ERROR ZabbixDB.backup завершился ошибкой корования директории '/etc/apache2/'!!! \nHost:<Имя хоста забикс>}"
        return 1
fi

tar -zcvf <Каталог для backup>/$b_time.zabbix-backup.tar.gz <Каталог для backup>/backup
tmp=$(echo $?)
if [ $tmp != 0 ]; then
        curl -s https://api.telegram.org/bot${TOKEN}/sendMessage -d "chat_id=$CHAT" -d "text=ERROR ZabbixDB.backup завершился ошибкой сжатия backup!!! \nHost:<Имя хоста забикс>}"
        return 1
fi
$(scp <Каталог для backup>/$b_time.zabbix-backup.tar.gz <user>@<Имя хоста logger>:<Каталог для backup>/backups/myhome-zabbix/)
if [ $tmp != 0 ]; then
        curl -s https://api.telegram.org/bot${TOKEN}/sendMessage -d "chat_id=$CHAT" -d "text=ERROR ZabbixDB.backup завершился ошибкой загрузки backup на sysolog!!! \nHost:<Имя хоста забикс>}"
        return 1
fi

